# PebblePINChanger
A Pebble app to change the PIN lock to your University of Maine dorm room.

Written in Pebble.js and built on top of the UMaine Service API created by nh-99 (https://github.com/nh-99/UMaine-Service-API)
